# Docker Registrys

In diesem Repository habe ich eines meiner Projekte, die auf Docker laufen, kopiert und Zwei Versionen ins docker registry gepushet.

## Schritte

1. Repository erstellen
2. Projekt kopieren
3. mit docker login ins container registry einloggen
4. erste version builden und pushen
5. kleine Änderung erfassen
6. zweite version builden und pushen
